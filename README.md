# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Scope ###
	
	This is a games plattform to which I'll add more games
	

### How do I get set up? ###

Ensure you have node up to date as it is necessary to run the gameserver

Clone the repository

run npm install

run node ./gameServer

run ng serve to serve the app

run ng test to run tests


### Who do I talk to? ###

h.t.pires84@gmail.com


### Next steps ###

change store and service calls to parent components
Use marbles to test effects
Complete unit testing
Do E2E testing
Enforce responsiveness on all html elements
Allow for ngrx-translate
Allow for theming
