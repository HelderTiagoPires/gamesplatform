import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GamesPlatfformHttpService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  // Re Write to handle general service calls
  getHangman(): Observable<any> {
    return this.http.get<any>(`${this.url}/hangman/data`);
  }
  setHighscore(highscore): Observable<any> {
    return this.http.post<any>(`${this.url}/hangman/highscore`, {highscore});

  }
}
