import { By } from '@angular/platform-browser';
import { AppModule } from '../../../../app.module';
import { ComponentFixture, async, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { UserInputComponent } from './user-input.component';
import { HangmanModule } from '../../hangman.module';
import { DebugElement } from '@angular/core';

describe('UserInputComponent', () => {
  let component: UserInputComponent;
  let fixture: ComponentFixture<UserInputComponent>;
  let debugElement: DebugElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        HangmanModule
      ]
    });
    fixture = TestBed.createComponent(UserInputComponent);
    component = fixture.componentInstance;
  }));
  it('should create the UserInputComponent', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
  it('should render char array', () => {
    component.keyboard = ['k', 't'];
    fixture.detectChanges();
    debugElement = fixture.debugElement.query(By.css('.key'));
    expect(debugElement).toBeTruthy();
  });
  it('should trigger select letter event', () => {
    component.keyboard = ['k', 't'];
    spyOn(component.selectLetter, 'emit');
    fixture.detectChanges();
    debugElement = fixture.debugElement.query(By.css('.key'));
    debugElement.nativeElement.click();
    expect(component.selectLetter.emit).toHaveBeenCalled();
  });
});

