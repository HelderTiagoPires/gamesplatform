import { By } from '@angular/platform-browser';
import { AppModule } from '../../../../app.module';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { HangmanGraphicComponent } from './hangman-graphic.component';
import { HangmanModule } from '../../hangman.module';
import { DebugElement } from '@angular/core';
describe('HangmanGraphicComponent', () => {
  let component: HangmanGraphicComponent;
  let fixture: ComponentFixture<HangmanGraphicComponent>;
  let debugElement: DebugElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        HangmanModule
      ]
    });
    fixture = TestBed.createComponent(HangmanGraphicComponent);
    component = fixture.componentInstance;
  }));
  it('should create the HangmanGraphicComponent', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
  it('should not display any of progress-indicator children', () => {
    component.chances = 5;
    fixture.detectChanges();
    debugElement = fixture.debugElement.query(By.css('.scaffold'));
    expect(debugElement).toBeFalsy();
  });
  it('should display one of progress-indicator children', () => {
    component.chances = 4;
    fixture.detectChanges();
    debugElement = fixture.debugElement.query(By.css('.scaffold'));
    expect(debugElement).toBeTruthy();
  });
  it('should  display all of progress-indicator children', () => {
    component.chances = 0;
    fixture.detectChanges();
    const debugElements = fixture.debugElement.query(By.css('.progress-indicator')).childNodes;
    expect(debugElements.length).toBeGreaterThan(4);
  });
});
