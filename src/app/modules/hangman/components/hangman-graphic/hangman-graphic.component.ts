import { Store } from '@ngrx/store';
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-hangman-graphic',
  templateUrl: './hangman-graphic.component.html',
  styleUrls: ['./hangman-graphic.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HangmanGraphicComponent {
  @Input()  chances: number;
  
  constructor() {}
}
