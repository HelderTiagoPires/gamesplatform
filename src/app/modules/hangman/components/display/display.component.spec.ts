import { By } from '@angular/platform-browser';
import { AppModule } from '../../../../app.module';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { DisplayComponent } from './display.component';
import { HangmanModule } from '../../hangman.module';
describe('DisplayComponent', () => {
  let component: DisplayComponent;
  let fixture: ComponentFixture<DisplayComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        HangmanModule
      ]
    });
    fixture = TestBed.createComponent(DisplayComponent);
    component = fixture.componentInstance;
  }));
  it('should create the DisplayComponent', () => {
    component.loaded = true;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
  it('should render char boxes in length of word', () => {
    component.loaded = true;
    component.word = ['W', 'o', 'r', 'd'];
    fixture.detectChanges();
    const debugElements = fixture.debugElement.queryAll(By.css('.char'));
    expect(debugElements.length).toBe(4);
  });
  it('should render char boxes with Word text', () => {
    component.loaded = true;
    component.word = ['W', 'o', 'r', 'd'];
    fixture.detectChanges();
    const elements = fixture.debugElement.queryAll(By.css('.char')).map((value) => {
      return value.nativeElement.textContent.trim();
    });
    expect(elements).toEqual(['W', 'o', 'r', 'd']);
  });
  it('should render char boxes with blank text', () => {
    component.loaded = true;
    component.word = ['', '', '', ''];
    fixture.detectChanges();
    const elements = fixture.debugElement.queryAll(By.css('.char')).map((value) => {
      return value.nativeElement.textContent;
    });
    expect(elements).toEqual(['', '', '', '']);
  });
});

