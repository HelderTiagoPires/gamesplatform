import { Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DisplayComponent implements OnInit {
  title = "HANGMAN";

  @Input()  word: string[];
  @Input()  lettersChosen: string[];
  @Input()  loaded: boolean;

  constructor() {
  }
  ngOnInit() {

  }
}
