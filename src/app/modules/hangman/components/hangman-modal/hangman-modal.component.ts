import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  OnChanges
} from '@angular/core';
import { HighScore } from '../../models/hangman.model';

@Component({
  selector: 'app-hangman-modal',
  templateUrl: './hangman-modal.component.html',
  styleUrls: ['./hangman-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HangmanModalComponent {
  @Output() replay = new EventEmitter();
  @Output() submitHighscore = new EventEmitter();

  @Input() gameWon: boolean;
  @Input() chances: number;
  @Input() word: string[];
  @Input() highscores: HighScore[];
  @Input() isHighScore: boolean;
  @Input() score: number;

  @ViewChild('highscoreInput', {static: false}) input: ElementRef;
  
  constructor() {}
  onReplay() {
    this.replay.emit();
  }
  onSubmitHighscore($event: Event) {
    this.submitHighscore.emit({event: $event, player: this.input.nativeElement.value});
  }
}
