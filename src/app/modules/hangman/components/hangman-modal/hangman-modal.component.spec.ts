import { By } from '@angular/platform-browser';
import { AppModule } from '../../../../app.module';
import { ComponentFixture, async, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HangmanModalComponent } from './hangman-modal.component';
import { DebugElement, ElementRef } from '@angular/core';
import { HangmanModule } from '../../hangman.module';

describe('HangmanModalComponent', () => {
  let component: HangmanModalComponent;
  let fixture: ComponentFixture<HangmanModalComponent>;
  let debugElement: DebugElement;
  let button: ElementRef;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        HangmanModule
      ]
    });
    fixture = TestBed.createComponent(HangmanModalComponent);
    component = fixture.componentInstance;
  });
  it('should create the HangmanModalomponent', fakeAsync(() => {
    fixture.detectChanges();
    tick();
    expect(component).toBeTruthy();
  }));
  it('should render highscore input', fakeAsync(() => {
    component.isHighScore = true;
    fixture.detectChanges();
    tick();
    debugElement = fixture.debugElement.query(By.css('.highscore-send-highscore'));
    expect(debugElement).toBeTruthy();
  }));
  it('should not render highscore input', fakeAsync(() => {
    component.isHighScore = false;
    tick();
    fixture.detectChanges();
    debugElement = fixture.debugElement.query(By.css('.highscore-send-highscore'));
    expect(debugElement).toBeNull();
  }));

  //When Testing for the text we test for class-instead as to allow for latter ngx-translation and isolate from service testing
  it('should render Win text', fakeAsync(() => {
    component.gameWon = true;
    fixture.detectChanges();
    tick();
    const el = fixture.debugElement.queryAll(By.css('.win'));
    expect(el).toBeTruthy();
  }));
  it('should render Loose text', fakeAsync(() => {
    component.gameWon = false;
    fixture.detectChanges();
    tick();
    fixture.detectChanges();
    const el = fixture.debugElement.queryAll(By.css('.loose'));
    expect(el).toBeTruthy();
  }));
  it('should submit highscore', fakeAsync(() => {
    component.isHighScore = true;
    spyOn(component.submitHighscore, 'emit');
    fixture.detectChanges();
    tick();
    button = fixture.debugElement.query(By.css('.highscore-button'));
    button.nativeElement.click();
    fixture.detectChanges();
    expect(component.submitHighscore.emit).toHaveBeenCalled();
  }));
  it('should submit replay', fakeAsync(() => {
    spyOn(component.replay, 'emit');
    tick();
    button = fixture.debugElement.query(By.css('.replay'));
    button.nativeElement.click();
    expect(component.replay.emit).toHaveBeenCalled();
  }));
});
