import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { GameLogicService } from './services/game-logic.service';
import { HangmanGraphicComponent } from './components/hangman-graphic/hangman-graphic.component';
import { HangmanModalComponent } from './components/hangman-modal/hangman-modal.component';
import { HangmanComponent } from './hangman.component';
import { DisplayComponent } from './components/display/display.component';
import { UserInputComponent } from './components/user-input/user-input.component';

import { hangmanReducer } from './store/reducers/hangman.reducers';
import { HangmanEffects } from './store/effects/hangman.effects';
import { HangmanRoutingModule } from './hangman-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    HangmanComponent,
    DisplayComponent,
    HangmanGraphicComponent,
    HangmanModalComponent,
    UserInputComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    HangmanRoutingModule,
    StoreModule.forFeature('hangman', hangmanReducer),
    EffectsModule.forFeature([HangmanEffects])
  ],
  providers: [GameLogicService],
  exports: [HangmanComponent]
})
export class HangmanModule { }
