export interface HighScore {
    player: string;
    score: number;
}

export interface Hangman {
    word: string[];
    highscores: HighScore[];
    lettersChosen: string[];
    chances: number;
    gameWon: boolean;
    loaded: boolean;
    error: Error;
}