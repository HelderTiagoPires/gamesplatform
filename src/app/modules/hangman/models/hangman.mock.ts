export const mockHighScore = {
    player: 'mockPlayer2',
    score: 500
};

export const mockHangman = {
    word: ['t', 'e', 's', 't'],
    highscores: [
        {
            player: 'mockPlayer1',
            score: 500
        },
        {
            player: 'mockPlayer2',
            score: 500
        },
        {
            player: 'mockPlayer3',
            score: 500
        },
        {
            player: 'mockPlayer4',
            score: 500
        },
        {
            player: 'mockPlayer5',
            score: 500
        }
    ],
    lettersChosen: [],
    chances: 5,
    gameWon: false,
    loaded: true,
    error: null,
}