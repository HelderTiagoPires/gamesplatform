import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, HostListener } from '@angular/core';
import { Observable, Subscription, zip, merge, combineLatest } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.state';
import * as HangmanActions from './store/actions/hangman.actions';
import { GameLogicService } from './services/game-logic.service';
import { HighScore, Hangman } from './models/hangman.model';
import { GamesPlatfformHttpService } from '../../services/hangman-http.service';

@Component({
  selector: 'app-hangman',
  templateUrl: './hangman.component.html',
  styleUrls: ['./hangman.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class HangmanComponent implements OnInit, OnDestroy {

  hangman$: Observable<Hangman>;
  hangmanSubscription: Subscription;
  highscores$: Observable<HighScore[]>;
  mergedSubscription$: Subscription;

  displayedWord: string[];
  keyboard: string[];
  score: number;
  maxScore: number;
  isHighScore: boolean;

  @HostListener('selectedLetter') selectedLetter(key) {
    this.selectLetter(key);
    this.keyboard = this.removeKey(this.keyboard, key);
    this.cdr.markForCheck();
  }
   @HostListener('replay') replay() {
    this.store.dispatch(new HangmanActions.ReplayHangman());
    this.keyboard = this.createKeys();
    this.cdr.markForCheck();
  }
  
  @HostListener('submitHighscore', ['$event']) submitHighscore($event: any) {
    this.store.dispatch(new HangmanActions.SetHighscore({score: this.score, player: $event.player}));
    this.keyboard = this.createKeys();
    this.cdr.detectChanges();
  }
  constructor(private store: Store<AppState>, private cdr: ChangeDetectorRef, private gameLogic: GameLogicService) {}

  ngOnInit() {
    this.keyboard = this.createKeys();
    this.hangman$ = this.store.select(store => store.hangman);
    this.highscores$ = this.store.select(store => store.hangman.highscores);

    this.mergedSubscription$ = combineLatest(
      this.hangman$,
      this.highscores$
      ).subscribe(([hangman, highscores]: any) => {
          if (!this.maxScore) { this.maxScore = this.gameLogic.getMaxScore(hangman.word.length); }
          this.displayedWord = hangman.word.map((char) => {
            return this.gameLogic.checkIfFound(char, hangman.lettersChosen) ? char : '';
          });
          if (hangman.gameWon) {
            this.score =  this.gameLogic.checkScore(this.maxScore, (5 - hangman.chances) * 100);
            this.isHighScore = this.gameLogic.checkHighScore(highscores, this.score);
            this.cdr.detectChanges();
          } else {
            this.score = 0;
            this.isHighScore = false;
            this.cdr.detectChanges();
          }
    });
    this.store.dispatch(new HangmanActions.LoadHangman());
  }
  ngOnDestroy() {
    this.hangmanSubscription.unsubscribe()
    // this.loadedSubscription.unsubscribe();
    // this.lettersChosenSubscription.unsubscribe();
    // this.chancesSubscription.unsubscribe();
    // this.mergedSubscription$.unsubscribe();
  }
  createKeys(): string[] {
    let out = [];
    //add to keyboard A - Z
    for (let i = 65; i < 90; i++ ) {
      out.push(String.fromCharCode(i));
    }
    //add to keyboard 0 - 9
    for (let i = 48; i < 57; i++ ) {
      out.push(String.fromCharCode(i));
    }
    //add to keyboard @ & -
    out.push(String.fromCharCode(64));
    out.push(String.fromCharCode(45));
    return out;
  }
  removeKey(keyboard: string[], target) {
    return keyboard.filter((key) => key !== target);
  }
  selectLetter(key: string) {
    this.store.dispatch(new HangmanActions.SelectLetter(key));
  }
}
