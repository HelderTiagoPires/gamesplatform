import { By } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { AppModule } from '../../app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { HangmanModule } from './hangman.module';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';

import { StoreModule, Store } from '@ngrx/store';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { AppState } from '../../app.state';
import { mockHangman } from '../../modules/hangman/models/hangman.mock';
import { HangmanComponent } from './hangman.component';
import { routes } from '../../app-routing.module';
import { initialState } from './store/reducers/hangman.reducers';
import { DebugElement } from '@angular/core';
import { GameLogicService } from './services/game-logic.service';

describe('HangmanComponent', () => {
  let component: HangmanComponent;
  let fixture: ComponentFixture<HangmanComponent>;
  let store: MockStore<AppState>;
  let service: GameLogicService;
  let debugElement: DebugElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        AppModule,
        HangmanModule,
        StoreModule.forRoot({})
      ],
      providers:[{ provide: APP_BASE_HREF, useValue: '/' }, provideMockStore({initialState})],
    });
    service = TestBed.get(GameLogicService);
    store = TestBed.get(Store);
    fixture = TestBed.createComponent(HangmanComponent);
    component = fixture.componentInstance;
  }));
  afterEach(() => {
    store.setState({ hangman: mockHangman});
  });
  it('should create the HangmanComponent', () => {
    store.setState({ hangman: mockHangman});
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
  it('should render game', () => {
    store.setState({ hangman: mockHangman});
    fixture.detectChanges();
    debugElement = fixture.debugElement.query(By.css('.hangman-game'));
    expect(debugElement).toBeTruthy();
  });
  it('should render modal', () => {
    mockHangman.gameWon = true;
    store.setState({ hangman: mockHangman});
    fixture.detectChanges();
    debugElement = fixture.debugElement.query(By.css('.hangman-modal'));
    expect(debugElement).toBeTruthy();
  });
  it('should render modal', () => {
    mockHangman.chances = 0;
    store.setState({ hangman: mockHangman});
    fixture.detectChanges();
    debugElement = fixture.debugElement.query(By.css('.hangman-modal'));
    expect(debugElement).toBeTruthy();
  });
});
