import { SetHighscore } from '../actions/hangman.actions';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType} from '@ngrx/effects';
import * as hangmanActions from '../actions/hangman.actions';
import { Observable, of } from 'rxjs';
import { map, catchError, mergeMap, mapTo, withLatestFrom } from 'rxjs/operators';
import {
  LoadHangman,
  LoadHangmanSuccess,
  LoadHangmanFailed,
  DecrementChances,
  SelectLetter,
  NoopAction,
  SetGameWon
} from '../actions/hangman.actions';
import { AppState } from '../../../../app.state';
import { ReplayHangman } from '../actions/hangman.actions';
import { GameLogicService } from '../../services/game-logic.service';
import { GamesPlatfformHttpService } from '../../../../services/hangman-http.service';
@Injectable()
export class HangmanEffects {

    // Listen to Load action and retunrs success or error
    @Effect()
    loadHangman$: Observable<any> = this.actions$
    .pipe(
      ofType<LoadHangman>(hangmanActions.LOAD_HANGMAN),
      mergeMap(() => {
        return this.GamesPlatfformHttpService.getHangman().pipe(
          map(response => {
            return new LoadHangmanSuccess(response);
          }),
          catchError(error => of(new LoadHangmanFailed(error)))
          );
        })
      );

    // Listens to letter selection and decrements chances if not found in word
    @Effect()
    decrementChances$: Observable<any> = this.actions$
    .pipe(
      ofType<SelectLetter>(hangmanActions.SELECT_LETTER),
      withLatestFrom(this.store$),
      map((state) => {
        if (this.gameLogic.checkIfFound(state[0].payload, state[1].hangman.word)) {
          return new NoopAction();
        } else {
          return new DecrementChances();
        }
      })
    );

    // Listens to letter selection to set gameWon condition
    @Effect()
    setGameWon$: Observable<any> = this.actions$
    .pipe(
      ofType<SelectLetter>(hangmanActions.SELECT_LETTER),
      withLatestFrom(this.store$),
      map((state) => {
        const found = state[1].hangman.word.filter(
          (char) =>
            this.gameLogic
          . checkIfFound(char, state[1].hangman.lettersChosen)
          );
        if (found.length === state[1].hangman.word.length) {
          return new SetGameWon(true);
        } else if (state[1].hangman.chances < 1) {
          return new SetGameWon(false);
        } else {
          return new NoopAction();
        }
      })
    );

    // Listen to replay action as to reload data
    @Effect()
    replayGame$: Observable<any> = this.actions$
    .pipe(
      ofType<ReplayHangman>(hangmanActions.REPLAY_HANGMAN),
      mapTo(new LoadHangman())
    );

    // Listen to Set Highscore action send to server and replay
    @Effect()
    setHighscore$: Observable<any> = this.actions$
    .pipe(
      ofType<SetHighscore>(hangmanActions.SET_HIGHSCORE),
      map((action) => {
        this.GamesPlatfformHttpService.setHighscore(action.payload).subscribe(
          value => console.log('post value', value)
        );
        return new ReplayHangman();
        }),
      );
    constructor(
      private store$: Store<AppState>,
      private actions$: Actions,
      private GamesPlatfformHttpService: GamesPlatfformHttpService,
      private gameLogic: GameLogicService
      ) {}
}
