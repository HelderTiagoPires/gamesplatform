import { provideMockActions } from '@ngrx/effects/testing';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { hangmanReducer, initialState } from './hangman.reducers';
import { Actions } from '../actions/hangman.actions';
import { mockHangman } from '../../models/hangman.mock';

let actions$: Observable<Action>;
let action: Actions;
describe('default', () => {
    TestBed.configureTestingModule({
        providers: [provideMockActions(() => actions$)],
      });
    it('should return init state', () => {
      const noopAction = {type: 'NOOP'} as any;
      const result = hangmanReducer(undefined, noopAction);
      expect(result).toBe(initialState);
    });
  });
describe('Load Game', () => {
    describe('Load Hangman', () => {
        it('should toggle loading state', () => {
        action = {type: '[HANGMAN] Load'};
        const result = hangmanReducer(initialState, action);
        expect(result).toEqual({
            ...initialState,
            error: undefined,
            loaded: false
            });
        });
    });
    describe('Load Hangman Success', () => {
        it('should toggle loading state', () => {
        action = {type: '[HANGMAN] Load Success', payload: initialState};
        const result = hangmanReducer(initialState, action);
        expect(result).toEqual({
            ...initialState,
            error: undefined,
            loaded: true
            });
        });
    });
    describe('Load Hangman Failed', () => {
        it('should toggle loading state', () => {
            const error = new Error();
            action = {type: '[ERROR] Load Failed', payload: error};
            const result = hangmanReducer({...initialState, loaded: true}, action);
            expect(result).toEqual({
                ...initialState,
                error,
                loaded: true
            });
        });
    });
    describe('Replay Hangman', () => {
        it('should return initial state', () => {
            action = {type: '[HANGMAN] Replay'};
            const result = hangmanReducer(initialState, action);
            expect(result).toEqual(initialState);
        });
    });
    describe('Select Letter', () => {
        it('should add selected letter to selected', () => {
            mockHangman.lettersChosen = [];
            action = {type: '[LETTER] Select', payload: 't'};
            const result = hangmanReducer(mockHangman, action);
            expect(result).toEqual({
                ...mockHangman,
                 lettersChosen: ['t']
            });
        });
    });

    describe('Decrement Chances', () => {
        it('should decrement chances by one', () => {
            const targetChances = mockHangman.chances - 1;
            action = {type: '[CHANCES] Decrement'};
            const result = hangmanReducer(mockHangman, action);
            expect(result).toEqual({
                ...mockHangman,
                chances: targetChances
            });
        });
    });
    describe('Set Highscore', () => {
        it('should return initial state', () => {
            action = {type: '[HIGHSCORE] Set', payload: initialState.highscores[0]};
            const result = hangmanReducer(initialState, action);
            expect(result).toEqual(initialState);
        });
    });
    describe('Set GameWon', () => {
        it('should return initial state', () => {
            action = {type: '[GAMEWON] Set', payload: true};
            const result = hangmanReducer(initialState, action);
            expect(result).toEqual({
                ...initialState,
                gameWon: true
            });
        });
    });
});
