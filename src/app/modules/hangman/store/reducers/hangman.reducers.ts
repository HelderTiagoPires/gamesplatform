import { Action } from '@ngrx/store';
import { Hangman } from '../../models/hangman.model';
import * as HangmanActions from '../actions/hangman.actions';
import { SELECT_LETTER } from '../actions/hangman.actions';

export const initialState: Hangman = {
    word: [],
    highscores: [],
    lettersChosen: [],
    chances: 5,
    gameWon: false,
    loaded: false,
    error: undefined
};

export function hangmanReducer(state: Hangman = initialState, action: HangmanActions.Actions) {
    switch (action.type) {
        case HangmanActions.SELECT_LETTER:
            // Adds selected letter to found or iterates letters failed
            return {...state, lettersChosen: [ ...state.lettersChosen, action.payload]};
        case HangmanActions.LOAD_HANGMAN:
            // Listens to server response
            return {...state , loaded: false};
        case HangmanActions.LOAD_HANGMAN_SUCCESS:
            // Listens to server response
            return {...state, word: action.payload.word , highscores: action.payload.highscores, loaded: true, chances: 5};
        case HangmanActions.LOAD_HANGMAN_FAILED:
            return  {...state, error: action.payload};
        case HangmanActions.SET_HIGHSCORE:
            // Set Highscore
            return state;
        case HangmanActions.DECREMENT_CHANCES:
            // Decrements chances
            return {...state , chances: --state.chances};
        case HangmanActions.SET_GAMEWON:
            // Sets if game is won or lost 
            return {...state , gameWon: action.payload};
        case HangmanActions.REPLAY_HANGMAN:
            return initialState;
        default:
            return state;
    }
}