import { Action } from '@ngrx/store';
import { Hangman } from '../../models/HangMan.model';
import { HighScore } from '../../models/hangman.model';

export const LOAD_HANGMAN = '[HANGMAN] Load';
export const LOAD_HANGMAN_SUCCESS = '[HANGMAN] Load Success';
export const LOAD_HANGMAN_FAILED = '[ERROR] Load Failed';
export const REPLAY_HANGMAN = '[HANGMAN] Replay';
export const SET_HIGHSCORE = '[HIGHSCORE] Set';
export const SELECT_LETTER = '[LETTER] Select';
export const SET_GAMEWON = '[GAMEWON] Set';
export const DECREMENT_CHANCES = '[CHANCES] Decrement';
export const NO_OP_ACTION = '[Noop] Action';

export class LoadHangman implements Action {
    readonly type =  LOAD_HANGMAN;
    constructor() {}
}

export class LoadHangmanSuccess implements Action {
    readonly type =  LOAD_HANGMAN_SUCCESS;
    constructor(public payload: Hangman) {}
}

export class LoadHangmanFailed implements Action {
    readonly type =  LOAD_HANGMAN_FAILED;
    constructor(public payload: Error) {}
}

export class ReplayHangman implements Action {
    readonly type =  REPLAY_HANGMAN;
}
export class SelectLetter implements Action {
    readonly type =  SELECT_LETTER;
    constructor(public payload: string) {}
}
export class DecrementChances implements Action {
    readonly type =  DECREMENT_CHANCES;
}
export class SetHighscore implements Action {
    readonly type =  SET_HIGHSCORE;
    constructor(public payload: HighScore) {
    }
}
export class SetGameWon implements Action {
    readonly type =  SET_GAMEWON;
    constructor(public payload: boolean) {
    }
}
export class NoopAction implements Action {
    readonly type = NO_OP_ACTION;
}
export type Actions = SelectLetter | LoadHangman | LoadHangmanSuccess | LoadHangmanFailed | ReplayHangman | DecrementChances | SetHighscore | SetGameWon | NoopAction;