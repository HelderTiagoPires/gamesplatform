import { Injectable } from '@angular/core';
import { HighScore } from '../models/hangman.model';


@Injectable({
  providedIn: 'root'
})
export class GameLogicService {

  constructor() {} //private store: Store<AppState>
  public checkIfFound(char: string, array: string[]): boolean {
    const arrayUpperCase = array.map((item) => item.toUpperCase());
    return (arrayUpperCase.indexOf(char.toUpperCase()) > -1);
  }
  public getMaxScore(base) {
    return base * 100;
  }
  public checkScore(maxScore, current) {
    return  maxScore - current;
  }
  public checkHighScore(highscores: HighScore[], current: number): boolean {
    let out = false;
    highscores.forEach((score) => {
      if (score.score < current) {
        out = true;
        return;
      }
    });
    return out;
  }
}

