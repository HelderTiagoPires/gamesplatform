// import { TestBed, async } from '@angular/core/testing';

// import { GameLogicService } from './game-logic.service';
// import { HighScore } from '../modules/hangman/models/hangman.model';
// import { initialState } from '../modules/hangman/store/reducers/hangman.reducers';
// import { AppModule } from '../app.module';
// import { provideMockStore } from '@ngrx/store/testing';
// import { mockHangman } from '../modules/hangman/models/hangman.mock';
// import { StoreModule } from '@ngrx/store';

// describe('GameLogicService', () => {

//   let highscores: HighScore[];
//   let service: GameLogicService;

//   beforeEach(async((() => {
//     TestBed.configureTestingModule({
//       imports: [
//         AppModule,
//         StoreModule.forRoot({})
//       ],
//       providers:[ provideMockStore({initialState})],
//     });
//     service = TestBed.get(GameLogicService);
//   })));
//   it('should be created', () => {
//     expect(service).toBeTruthy();
//   });
//   it('should call checkIfFound() and find character in array', () => {
//     const charArray = ['r', 'r', 'a'];
//     const char = 'r';
//     const result = service.checkIfFound(char, charArray);
//     expect(result).toBe(true, 'character not found but should');
//   });
//   it('should call checkIfFound() and not find character in array', () => {
//     const charArray = ['r', 'r', 'a'];
//     const char = 'x';
//     const result = service.checkIfFound(char, charArray);
//     expect(result).toBe(false, 'character found but shouldnt');
//   });
//   it('should call getMaxScore() and return max score', () => {
//     const result = service.getMaxScore(5);
//     expect(result).toBe(500, 'max score returned wrong');
//   });
//   it('should call checkHighScore() and check for highscore and return true', () => {
//     highscores = mockHangman.highscores;
//     const result = service.checkHighScore(highscores, 50000);
//     expect(result).toBeTruthy();
//   });
//   it('should call checkHighScore() and check for highscore and return false', () => {
//     highscores = mockHangman.highscores;
//     const result = service.checkHighScore(highscores, 1);
//     expect(result).toBeFalsy();
//   });
//   // it('should call selectLetter() and dispatch action', () => {
//     // highscores = initialState.highscores;
//     // const result = service.checkHighScore(highscores, 0);
//     // expect(result).toBe(true, 'max score returned wrong');
//   // });
//   it('should call createKeys() and return string[]', () => {
//     const result = service.createKeys();
//     expect(result.length).toBeGreaterThan(1, 'createKeys() returned wrong');
//   });
//   it('should call removeKey() and return char[] without target', () => {
//     const chars = ['r', 'x', 'a'];
//     const char = 'r';
//     const result = service.removeKey(chars, char);
//     expect(result.length).toEqual(2, 'removeKey() returned wrong');
//   });
// });
