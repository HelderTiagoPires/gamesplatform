import { NgModule } from '@angular/core';

// Shared modal meant to hold abstracted components

@NgModule({
  declarations: [],
  imports: [],
  providers: [],
})
export class SharedModule { }
