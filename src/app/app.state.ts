import {Hangman} from './modules/hangman/models/hangman.model';

export interface AppState {
    readonly hangman: Hangman;
}