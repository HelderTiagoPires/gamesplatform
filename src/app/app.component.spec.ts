import { initialState } from './modules/hangman/store/reducers/hangman.reducers';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { routes } from './app-routing.module';

import { GamesPlatfformHttpService } from './services/hangman-http.service';
import { APP_BASE_HREF } from '@angular/common';
import { AppModule } from './app.module';

import { StoreModule, Store } from '@ngrx/store';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { AppState } from './app.state';
import { mockHangman } from './modules/hangman/models/hangman.mock';
describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let store: MockStore<AppState>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        AppModule,
        StoreModule.forRoot({})
      ],
      providers: [ GamesPlatfformHttpService, { provide: APP_BASE_HREF, useValue: '/' }, provideMockStore({initialState})]
    });
    store = TestBed.get(Store);
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

  }));
  afterEach(() => {
    store.setState({ hangman: mockHangman});
  });
  it('should create the app', () => {
    store.setState({ hangman: mockHangman});
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
