import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


export const routes: Routes = [
 {path: '',
 redirectTo: '/hangman',
 pathMatch: 'full'
},
 {path: 'hangman', loadChildren: () => import('./modules/hangman/hangman.module').then(m => m.HangmanModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
