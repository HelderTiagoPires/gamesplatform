var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json())

const util = require('util')
const fs = require('fs');
const PORT = 8080;
const read = util.promisify(fs.readFile);
const write = util.promisify(fs.writeFile);
const wordsList = ['3dhubs', 'marvin', 'print', 'filament', 'order', 'layer'];

function getRandomWord() {
  return wordsList[Math.floor(Math.random() * wordsList.length)];
};
function setHighScore(highscores, request) {
  for(let i=0; i<highscores.length; i++) {
    if (request.score > highscores[i].score) {
      highscores[i] = request;
      return
    }    
  }
  return highscores;
}
function main() {
  let highscores = []
  read('./highscores.json')
    .then(value => highscores = JSON.parse(value))
    .catch(error => console.log(error));
  
  // listen to requests
  app.listen(PORT, () => {
    console.log(`Server running at: http://localhost:${PORT}/`);
  });
  //Set Headers
  app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  })
  //Get HangmanData
  app.get('/hangman/data', (req, res) => {
    read('./highscores.json')
    // .then(value => highscores = JSON.parse(value))
    .then(value => {
      res.send({
        word: 'word'.split(''),
        // word: getRandomWord().split(''),
        highscores: JSON.parse(value)
      });
    })
    .catch(error => console.log(error));
  });
  //Try Post Highscore
  app.post('/hangman/highscore/', (req, res) => {

    console.log('req.body:', req.body)
    let newscore = JSON.stringify(setHighScore(highscores, req.body.highscore))
    write('./highscores.json', newscore)
  });
}

main();